# Humanoid Robot Workshop

In this workshop you are going to learn about humanoid robots, and some tools that we use at
ARCOS-Lab to work with them. Here are some topics that we are going to discuss:

  - The humanoid robot  (Why?) (Federico)
  - YARP (Daniel)
  - Kinematics (Daniel)
  - Vector Fields (Daniel, Federico)
  - vfclik architecture (Federico)
  - Using the simulator (Daniel)
  - Moving the real robot (Daniel, Federico)
