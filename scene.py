#!/usr/bin/python
# Copyright (c) 2019 Universidad de Costa Rica
# Author: Daniel Garcia Vaglio
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys
import argparse
import signal
from numpy import array, identity, pi
from time import time, sleep
import datetime
import pickle

import yarp

from pyrovito.pyrovito_utils import Roboviewer_objects
from cmoc.robot.hand_sim_handler import Hand
from vfclik.handlers import HandleArm, HandleBridge
from arcospyu.kdl_helpers import my_diff, my_adddelta
from cmoc.objects.sliding.utils import move_robot, finger_joint_move
from arcospyu.config_parser.config_parser import import_config
from arcospyu.rawkey.rawkey import Raw_key, Keys
from os.path import expanduser

namespace = "/arcosbot-real"
base_name = "/scene"
view_objects = Roboviewer_objects(
    base_name, namespace+"/lwr/roboviewer", counter=300)


table_pose = identity(4)
table_pose[:3,3] = [0.9, 0.0, 0.77]

table_id = view_objects.create_object("box")
view_objects.send_prop(table_id, "scale",
                       [0.61, 1.215, 0.005])
view_objects.send_prop(table_id, "color", [0.3, 0.3, 0.3])
view_objects.send_prop(table_id, "timeout", [-1])
view_objects.send_prop(table_id, "pose",
                       list(table_pose.flatten()))


object_pose = identity(4)
object_pose[:3, 3] = [1.0, 0.0, 0.77]

object_id = view_objects.create_object("cylinder")
view_objects.send_prop(object_id, "scale",
                       [0.5, 0.5, 0.10])
view_objects.send_prop(object_id, "timeout", [-1])
view_objects.send_prop(object_id, "color", [1., 0., 0.])
view_objects.send_prop(object_id, "pose",
                       list(object_pose.flatten()))


while True:
    sleep(0.01)
