#!/usr/bin/python
# Copyright (c) 2019 Universidad de Costa Rica
# Author: Daniel Garcia Vaglio
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys
import argparse
from os.path import expanduser
from numpy import array, identity, pi
from time import time, sleep

import yarp

from cmoc.robot.hand_sim_handler import Hand
from vfclik.handlers import HandleArm, HandleBridge
from arcospyu.kdl_helpers import my_diff, my_adddelta
from cmoc.objects.sliding.utils import move_robot, finger_joint_move
from arcospyu.config_parser.config_parser import import_config



# Define yarp port names
namespace = "/arcosbot-real"
arm_portbasename = namespace + "/lwr/right"
base_name = "/robot_controller"

# Arm handler, we just need to pass the names of the yarp ports to
# be used.
arm = HandleArm(arm_portbasename, handlername=base_name + "/arm")

# Arm bridge: Used to bridge simulation to reality
arm_bridge = HandleBridge(
    arm_portbasename,
    handlername=(base_name + "/arm_bridge"),
    torso=False)  # Disable movable torso

# Import hand configuration
config_hands = import_config(
    expanduser("~/local/src/robot_descriptions/arcosbot/"+
               "kinematics/sahand/hands_kin.py"
    )
)

# Hand handler
hand = Hand(
    config_hands,
    handedness="right",
    portprefix=namespace+"/robot_controller",
    sahand_port_name_prefix=namespace+"/sahand",
    sahand_number=0)  # use first available hand


# Configure the arm
# Set the stiffnes values for impedance control
max_stiffness_arm = array([240.0]*7)  # a value for each joint
arm.set_stiffness(max_stiffness_arm)
# Set the type of controller: joint_space or cartesian
arm_bridge.cartesian_controller()

# Configure the hand
# Set speed and stiffness for each finger
finger_speed = [pi / 2.0] * 3
finger_max_stiffness = [28.64, 28.64, 8.59]
for finger in range(0,5):  # configure the 5 fingers
    hand.set_params(
        finger, finger_speed + finger_max_stiffness)

hand.update_controller_params()

# Define the required joint angle in degrees
joint_angles = array([0.0,  # Abductor
                      10.0,  # Proximal
                      10.0] # Distal
)

# Move the Index
finger_joint_move(
    hand,  # Defien the hand handler to use
    1,  # The index is the second finger
    joint_angles,  # We pass a numpy array
    wait=10.,  # How many seconds to wait for the action to complete
    goal_precision=5.  # The required precision
)

# Move the thumb. We use 4 joints 
finger_joint_move(
    hand, 0,  # thumb is the first finger
    array([0.0, 0.0, 10.0, 10.0]), # value in degrees for each joint
    wait=10.0,  
    goal_precision=5.0  
)



# Get the current pose of the arm_bridge
# The getPose return a 16x1 list, so we have to convert it into a
# 4x4 homogeneous matrix
pose_list = arm.getPose(blocking=False)
pose = array(pose_list).reshape(4, 4)


# Define How much we want the arm to move in cartesian space
x_trans = 0.1
y_trans = 0.0
z_trans = 0.1
x_rot = 10 * pi / 180.0
y_rot = 0.0
z_rot = 0.0

diff = array([x_trans, y_trans, z_trans, x_rot, y_rot, z_rot])

# Calculate the new pose
cmd = my_adddelta(pose, diff, 1)

# Move the arm
move_robot(
    hand, # Specify the Hand handler attached to the arm
    1,  # Specify a reference finger
    arm,  # The arm handler
    cmd,  # Commanded pose
    [0.03, 5.0 * pi / 180.0],  # translation, rotation precision
    wait=10.0  # How much to wait
)

# The script is done, but the handlers have started some threads in then
# Background, so the script is not goint to exit. For now we are going to
# Force the exit.
del(arm)
del(arm_bridge)
del(hand)
# sys.exit()
